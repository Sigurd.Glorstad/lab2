package INF101.lab2;
import java.util.ArrayList;
import java.util.NoSuchElementException;
import java.util.List;


public class Fridge implements IFridge {
	int maxSize = 20;
    ArrayList<FridgeItem> Items = new ArrayList<FridgeItem>();

	public Fridge(){}
    public int nItemsInFridge(){
        return Items.size();
    }
    public int totalSize(){
        return maxSize;
    }
    public boolean placeIn(FridgeItem item){
        if(nItemsInFridge()<totalSize()){
            Items.add(item);
            return true;
        }else{
            return false;
        }
    }
    public void takeOut(FridgeItem item){
        if(Items.contains(item)){
            Items.remove(item);
        }else{
            throw new NoSuchElementException();
        }
        
    }
    public void emptyFridge(){
        Items.removeAll(Items);
    }
    public List<FridgeItem> removeExpiredFood(){
        List<FridgeItem> expiredItems = new ArrayList<FridgeItem>();
        for(FridgeItem i:Items){
            if(i.hasExpired()){
                expiredItems.add(i);    
            }
        }
        Items.removeAll(expiredItems);
        return expiredItems;
    }
    
}